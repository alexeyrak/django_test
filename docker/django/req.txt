Django==2.0.3
djangorestframework==3.8.0
celery==3.1.26.post2
mysqlclient==1.3.12
flower==0.9.2
django-suit==0.2.26
