const path = require("path");
const webpack = require("webpack");
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const helpers = {
    root(args) {
        args = Array.prototype.slice.call(arguments, 0);
        return path.join.apply(path, [__dirname].concat(args));
    }
};

const ExtractAppStyle = new ExtractTextPlugin('app.css');
const ExtractAdminStyle = new ExtractTextPlugin('admin.css');


module.exports = {
     watchOptions: {
        poll: true
    },
    entry: {
        app: './front/scripts/app.js',
        'users.admin': './front/scripts/admin/users.js'
    },
    output: {
        path: path.resolve(__dirname, "static"),
    },
    devtool: "source-map",
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        plugins: [
                            'babel-plugin-transform-runtime'
                        ],
                        presets: [
                            'babel-preset-env',
                            'babel-preset-es2015',
                            'babel-preset-stage-0',
                        ]
                    }
                }
            },
            {
                test: /\.css$/,
                exclude: helpers.root('front', 'admin_style'),
                use: ExtractAppStyle.extract({
                    fallback: "style-loader",
                    use: "css-loader?sourceMap"
                })
            },
            {
                test: /\.css$/,
                include: helpers.root('front', 'admin_style'),
                use: ExtractAdminStyle.extract({
                    fallback: "style-loader",
                    use: "css-loader?sourceMap"
                })
            },
        ]
    },
    plugins: [
        ExtractAppStyle,
        ExtractAdminStyle,
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            JQuery: 'jquery',
            'window.jQuery': 'jquery',
            'moment': 'moment'
        }),
    ]
};