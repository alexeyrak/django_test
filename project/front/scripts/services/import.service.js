import {BaseService} from "./base/base.serivce";

let instance = null;
export class ImportService extends BaseService{
    constructor(){
        if(!instance) {
            super();
            instance = this;
        }
        return instance;
    }

    get(id) {
        return this.http.get(`/imports/${id}/`);
    }
}