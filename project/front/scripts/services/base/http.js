let instance = null;
export class Http {
    constructor() {
        if(!instance) {
            this.$ = $;
            instance = this;
        }
        return instance;
    }

    get(url, data){
        return this.$.get(url, data);
    }

    post(url, data){
        return this.$.post(url, data);
    }

    load(url, data) {
        let formData = new FormData(data);
        return $.ajax({
            type: 'POST',
            url: url,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
        });
    }
}
