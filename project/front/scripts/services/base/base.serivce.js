import {Http} from "./http";

export class BaseService {
    constructor() {
        this.http = new Http();
    }
}