import {BaseService} from "./base/base.serivce";

let instance = null;
export class OrderService extends BaseService {
    constructor() {
        super();
        // if(instance) {
        //     super();
        //     instance = this;
        // }
        //
        // return instance;
    }

    async increase(product_id) {
        return this.http.post('/order/increase', {
            product: product_id,
            csrfmiddlewaretoken: $('[name="csrfmiddlewaretoken"]').val()
        });
    }

    async change(product_id, amount) {
        return this.http.post('/order/change', {
            product: product_id,
            amount,
            csrfmiddlewaretoken: $('[name="csrfmiddlewaretoken"]').val()
        });
    }
}