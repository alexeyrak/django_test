let instance = null;
class AuthService extends BaseService {
    constructor() {
        if(instance) {
            super();
            instance = this;
        }
        return instance;
    }

    async login(data) {
        return this.http.post('/auth/sign_in', data)
    }
}