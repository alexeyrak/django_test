import {BaseService} from "./base/base.serivce";

let instance = null;
export class FileUploadService extends BaseService{
    constructor(){
        if(!instance) {
            super();
            instance = this;
        }
        return instance;
    }

    load(data, url) {
        return this.http.load(url, data);
    }
}