import {OrderList} from "./component/order-list";

require("babel-core/register");
require("babel-polyfill");

import 'bootstrap/dist/css/bootstrap.min.css'
import '../styles/app.css';
import 'jquery';
import 'bootstrap';
import {FormControl} from './modules/forms';
import {ProductList} from './component/product-list'

window['$'] = $;

$(() => {
    new FormControl('#login-form', res => location.pathname = window.URLS.PRODUCTS);

    new ProductList();
    new OrderList();
});
