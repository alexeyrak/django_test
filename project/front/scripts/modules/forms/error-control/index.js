export class ErrorControl {
    constructor(form, selector = '.validation-error', nameAttr = 'validation-name') {
        this.$form = $(form);
        this.selector = selector;
        this.nameAttr = nameAttr;
        this.updateElementList();
    }

    updateElementList() {
        this._errElList = this.$form.find(this.selector);
        console.log(this._errElList);

        let errorElements = {};
        this._errElList
            .each((i, el) => {
                let $el = $(el);
                let name = $el.attr(this.nameAttr);
                errorElements[name] = $el;
            });
        this._errorElements = errorElements;
    }

    resetError(field) {
        if(this._errorElements[field]) {
            this._errorElements[field]
                .hide()
                .html('');
        }
    }

    resetErrors() {
        this._errElList
            .hide()
            .html('');
    }

    setErrors(errors) {
        Object.keys(this._errorElements)
            .forEach((key) => {
                if(errors[key]) {
                    this._errorElements[key]
                        .show()
                        .html(ErrorControl.errorListHTML(errors[key]));
                }
            });
    }

    static errorListHTML(list) {
        return list.map(message => `
            <div>${message}</div>
        `);
    }
}