import {ErrorControl} from './error-control'
import {Http} from "../../services/base/http";
export class FormControl {
    constructor(form, success, error) {
        this.success = success;
        this.error = error;
        this.http = new Http();
        this.$form = $(form);
        this.errorsControl = new ErrorControl(form);

        this._events();
    }

    _events() {
        this.$form
            .on('submit', e => {
                e.preventDefault();
                this.submit();
            })
            .on('change input', () => this.resetError('non_field_errors'));
        this.$form.find('[name]')
            .on('change input', e => this.resetError(e.target.name));
    }

    submit() {
        let method = this.$form.attr('method').toLowerCase();
        let url = this.$form.attr('action');
        this.http[method](url, this.getFormValue())
            .then((res) => {
                this.success && this.success(res);
            }, (res) => {
                this.errorsControl.setErrors(res.responseJSON);
                this.error && this.error(res);
            });
    }

    resetError(field) {
        this.errorsControl.resetError(field);
    }

    getFormValue() {
        let value = {};
        this.$form.find('[name]')
            .each((i, el) => {
                let $el = $(el);
                value[$el.attr('name')] = $el.val();
            });
        return value;
    }
}