import {OrderService} from "../services/order.service";

export class OrderList {
    constructor() {
        this.orderService = new OrderService();
        this._events();
    }

    _events() {
        $('.order-block')
            .on('change', '.counter', e => this.increase($(e.target)))
            .on('focus', '.counter', e => this.tempValue($(e.target)));
    }

    async increase($input) {
        let $productBlock = $input.closest('.product');
        let productId = $productBlock.data('product-id');
        try {
            let result = await this.orderService.change(productId, $input.val());
            this.tempValue($input);
            if(!result.amount) {
                $productBlock.remove();
            }
        } catch (e) {
            $input.val($input.data('temp'));
        }
    }

    tempValue($input) {
        $input.data('temp', $input.val());
    }
}