import {ErrorControl} from "../modules/forms/error-control";
import {FileUploadService} from "../services/file-upload.service";
import {ImportService} from "../services/import.service";

export class UserUploadFormControl {
    constructor(success, error) {
        this.success = success;
        this.error = error;
        this.fileUploadService = new FileUploadService();
        this.importService = new ImportService();
        this.$form = $('#users-import-form');
        this.errorsControl = new ErrorControl(this.$form);

        this.loader = new Loader(this.$form);
        this.message = new Message(this.$form);

        this._events();
    }

    _events() {
        this.$form
            .on('submit', e => {
                e.preventDefault();
                this.submit(e.target);
            })
            .on('change input', () => this.resetError('non_field_errors'));
        this.$form.find('[name]')
            .on('change input', e => this.resetError(e.target.name));
    }

    submit(form) {
        // let method = this.$form.attr('method').toLowerCase();
        let url = this.$form.attr('action');
        this.fileUploadService.load(form, url)
            .then((res) => {
                this.checkImport(res.import_id);
                this.success && this.success(res);
            }, (res) => {
                this.errorsControl.setErrors(res.responseJSON);
                this.error && this.error(res);
            });
    }

    resetError(field) {
        this.errorsControl.resetError(field);
    }

    checkImport(id) {
        this.loader.show();
        let timer = setInterval(() => {
            this.importService.get(id)
                .then((res) => {
                    if(res.status === 'success') {
                        this.message.show(res.message, Message.STATUSES.SUCCESS);
                        setTimeout(() => location.reload(), 10000)
                    } else if (res.status === 'failed') {
                        this.message.show(res.message, Message.STATUSES.ERROR);
                    }
                    if(res.status !== 'in_process') {
                        clearInterval(timer);
                    }
                    this.loader.hide()
                }, () => {
                    clearInterval(timer);
                    this.loader.hide();
                });
        }, 5000)
    }



    // getFormValue() {
    //     let value = {};
    //     this.$form.find('[name]')
    //         .each((i, el) => {
    //             let $el = $(el);
    //             value[$el.attr('name')] = $el.val();
    //         });
    //     return value;
    // }
}


class Message {
    static get STATUSES() {
        return {
            SUCCESS: 'alert-success',
            ERROR: 'alert-danger',
        }
    };
    constructor(container) {
        this.$container = $(container);
        this.$container.append('<pre class="alert"></pre>');
        this.$loader = this.$container.find('.alert').eq(0);
        this.$loader.hide();
    }

    show(message, status = Message.STATUSES.SUCCESS) {
        this.$loader.text(message);
        this.$loader.addClass(status);
        this.$loader.show();
    }

    hide() {
        this.$loader.hide();

        this.$loader.removeClass(Message.STATUSES.SUCCESS);
        this.$loader.removeClass(Message.STATUSES.ERROR);
    }
}

class Loader {
    constructor(container) {
        this.$container = $(container);
        this.$container.append('<div class="loader"></div>');
        this.$loader = this.$container.find('.loader').eq(0);
        this.$loader.hide();
        this.$container.css({
            'position': 'relative'
        });
    }

    show() {
        this.$loader.show();
    }

    hide() {
        this.$loader.hide();
    }
}