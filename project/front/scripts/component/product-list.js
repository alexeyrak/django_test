import {OrderService} from "../services/order.service";

export class ProductList {
    constructor() {
        this.orderService = new OrderService();
        this._events();
    }

    _events() {
        $('.product-list').on('click', '.increase-order-product', e => this.increase(e));
    }

    async increase(e) {
        let $btn = $(e.target);
        let productId = $btn.closest('.product').data('product-id');
        $btn.attr('disabled', true);
        await this.orderService.increase(productId);
        $btn.attr('disabled', false);
    }
}