import {UserUploadFormControl} from "../component/users-import-form";
import '../../admin_style/style.css';

$(() => {
    let form = new UserUploadFormControl(
        (res) => {
            localStorage.setItem('import', res.import_id);
        },
        res => console.log('error', res)
    );
    window['form'] = form;
});