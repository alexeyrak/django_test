from django.contrib.auth.decorators import user_passes_test
from django.urls import path
from .views import LoginView, SignInVIew, LogoutView

login_forbidden = user_passes_test(lambda u: u.is_anonymous(), '/')

urlpatterns = [
    path('login/', (LoginView.as_view()), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('sign_in/', SignInVIew.as_view()),
]
