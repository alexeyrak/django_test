from django.apps import AppConfig


class AuthConfig(AppConfig):
    name = 'app.site_auth'
