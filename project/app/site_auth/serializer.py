from django.contrib.auth import authenticate
from rest_framework import serializers


class AuthSerializer(serializers.Serializer):
    user = None
    email = serializers.EmailField(required=True)
    password = serializers.CharField(style={'input_type': 'password'})

    def validate_email(self, value):
        return value.lower()

    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')
        user = authenticate(email=email, password=password)

        if user:
            if not user.is_active:
                raise serializers\
                    .ValidationError('User is not activated. Check your email or use password recovery to log in')
        else:
            raise serializers.ValidationError('Email or password incorrect')

        self.user = user
        return attrs



