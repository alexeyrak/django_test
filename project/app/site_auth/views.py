from django.contrib.auth import login, logout
from django.shortcuts import redirect
from django.views.generic import TemplateView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from app.site_auth.serializer import AuthSerializer
from app.user.serializer import UserSerializer


class LoginView(TemplateView):
    template_name = 'app/login/index.html'


class LogoutView(APIView):
    permission_classes = (AllowAny,)

    def get(self, request):
        logout(request)
        return redirect('login')


class SignInVIew(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        serializer = AuthSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.user
        print(type(user))
        login(request, user)
        user_dict = UserSerializer(context={'request': request}, instance=user)
        return Response({'user': user_dict.data})
