from django.db import models


class ImportFile(models.Model):
    IN_PROCESS = 'in_process'
    SUCCESS = 'success'
    FAIlED = 'failed'

    STATUSES = (
        (IN_PROCESS, 'In process'),
        (SUCCESS, 'Success'),
        (FAIlED, 'Failed')
    )

    file = models.ForeignKey('file.File', null=True, blank=True, on_delete=models.DO_NOTHING)
    status = models.CharField(verbose_name='Status', choices=STATUSES, max_length=20, default=IN_PROCESS)
    message = models.CharField(verbose_name='Message', max_length=500, null=True, default=None)

    class Mete:
        verbose_name = 'Import'
