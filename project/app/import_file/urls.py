from rest_framework import routers
from app.import_file.views import ImportView

router = routers.SimpleRouter()
router.register(r'', ImportView, base_name='import')

urlpatterns = [
]

urlpatterns += router.urls
