from django.shortcuts import render

# Create your views here.
from rest_framework.generics import GenericAPIView
from rest_framework.mixins import RetrieveModelMixin
from rest_framework.viewsets import GenericViewSet

from .models import ImportFile
from .serializer import ImportSerializer


class ImportView(RetrieveModelMixin, GenericViewSet):
    queryset = ImportFile.objects.all()
    serializer_class = ImportSerializer
