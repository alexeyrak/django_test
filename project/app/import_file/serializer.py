from rest_framework import serializers

from .models import ImportFile


class ImportSerializer(serializers.ModelSerializer):
    class Meta:
        model = ImportFile
        fields = '__all__'
