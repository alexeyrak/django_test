from django.apps import AppConfig


class FileConfig(AppConfig):
    name = 'app.file'
