from django.db import models


class File(models.Model):
    file = models.FileField(verbose_name='File')

    class Meta:
        verbose_name = 'File'
