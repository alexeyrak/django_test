from rest_framework import serializers
from rest_framework.settings import api_settings

from app.file.models import File


class UserImportFileSerializer(serializers.ModelSerializer):
    def validate_file(self, file):
        if not file:
            raise serializers.ValidationError({api_settings.NON_FIELD_ERRORS_KEY: ['Is required']})
        if not file.name.endswith('.csv'):
            raise serializers.ValidationError({api_settings.NON_FIELD_ERRORS_KEY: ['File is not CSV type']})
        if file.multiple_chunks():
            raise serializers.ValidationError({
                api_settings.NON_FIELD_ERRORS_KEY: [
                    "Uploaded file is too big (%.2f MB)." % (file.size / (1000 * 1000),)]
            })
        return file

    class Meta:
        fields = ('file',)
        model = File
