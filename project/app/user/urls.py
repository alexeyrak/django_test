from django.urls import path
from rest_framework.routers import DefaultRouter

from app.user.views import ImportUsersView, UserView


router = DefaultRouter()
router.register(r'', UserView, base_name='user')

urlpatterns = [
    # path('test', TestView.as_view(), name='test'),
    path('import', ImportUsersView.as_view()),
]

urlpatterns += router.urls