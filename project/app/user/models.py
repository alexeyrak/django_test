from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.db import models


class CustomUserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, is_staff, is_superuser, **extra_fields):
        email = self.normalize_email(email)
        user = self.model(email=email, is_staff=is_staff, is_active=True, is_superuser=is_superuser,
                          **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email=None, password=None, **extra_fields):
        return self._create_user(email, password, False, False, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        return self._create_user(email, password, True, True, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    objects = CustomUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    id = models.AutoField(primary_key=True)
    first_name = models.CharField(verbose_name='First name', max_length=30, blank=False,)
    last_name = models.CharField(verbose_name='Last name', max_length=30, blank=False,)
    email = models.EmailField(verbose_name='Email', unique=True, blank=False,)
    created_at = models.DateTimeField(verbose_name='Creation date', auto_now_add=True)
    is_staff = models.BooleanField(verbose_name='Staff', default=False,)
    is_active = models.BooleanField(verbose_name='Active', default=True,)

    change_count = models.PositiveIntegerField(verbose_name='Change count', default=0)

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name

    class Meta:
        verbose_name = 'User'
