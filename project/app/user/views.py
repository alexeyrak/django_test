from django.views.generic import TemplateView
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.views import APIView

from app.file.serializer import UserImportFileSerializer
from app.import_file.models import ImportFile
from .models import User
from .permissions import IsAccountAdminOrReadOnly
from .serializer import UserCreationSerializer, UserSerializer
from .tasks import import_users_task


class TestView(TemplateView):
    template_name = 'app/test.html'


class ImportUsersView(APIView):
    model = User
    serializer_class = UserCreationSerializer
    # permission_classes = (permissions.IsAdminUser, )

    def post(self, request):
        file_serializer = UserImportFileSerializer(data=request.data)

        # return Response({
        #     api_settings.NON_FIELD_ERRORS_KEY: [error.get('message') for error in e.get_full_details()]
        # }, status=status.HTTP_400_BAD_REQUEST)
        file_serializer.is_valid(raise_exception=True)
        # file = file_ser.validated_data.get('file')
        file = file_serializer.save()
        import_file = ImportFile(file=file)
        import_file.save()
        print('import_users_task.delay({})'.format(import_file.id))
        import_users_task.delay(import_file.id)
        return Response({"import_id": import_file.id})


class UserView(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    # permission_classes = (IsAccountAdminOrReadOnly, )

