# Generated by Django 2.0.3 on 2018-07-09 20:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0003_auto_20180516_1427'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='change_count',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
