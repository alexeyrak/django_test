from django.db import transaction
from rest_framework import serializers
from app.import_file.models import ImportFile
from app.user.serializer import UserCreationSerializer, UserImportSerializer
from .models import User
from django.core.mail import send_mail
from app.celery import app


@app.task
def import_users_task(import_id):
    import_file = ImportFile.objects.get(id=import_id)
    serializer = UserImportSerializer(data={
        'file': import_file.file.file
    })
    try:
        serializer.is_valid(raise_exception=True)
    except serializers.ValidationError as e:
        return set_errors(import_file, e.detail.get('non_field_errors'))

    user_dict_list = serializer.validated_data.get('file')

    try:
        create_users(user_dict_list)
    except serializers.ValidationError as e:
        return set_errors(import_file, e.detail)

    import_file.status = ImportFile.SUCCESS
    import_file.message = 'Users successfully imported!'
    import_file.save()

@transaction.atomic()
def create_users(user_dict_list):
    has_error = False
    user_list = []
    error_message = ''
    for index, user_dict in enumerate(user_dict_list):
        user_dict['password'] = User.objects.make_random_password(8)
        user_serializer = UserCreationSerializer(data=user_dict)
        if user_serializer.is_valid():
            user_serializer.save()
            user_list.append(user_dict)
        else:
            error_message += 'User {}: \n\r'.format(index + 1)
            error_message += user_errors_to_string(user_serializer.errors)
            if not has_error:
                has_error = True
    if has_error:
        raise serializers.ValidationError(error_message)
    else:
        for user_dict in user_list:
            send_credentials_email.delay(user_dict)



def user_errors_to_string(errors):
    error_message = ''
    for field, messages in errors.items():
        error_message += '  {field}: \n\r    {message} \n\r' \
            .format(field=field, message='\n\r'.join(messages))
    return error_message


def set_errors(import_file, errors):
    import_file.status = ImportFile.FAIlED
    import_file.message = ''.join(errors)
    return import_file.save()


@app.task
def send_credentials_email(user):
    email_to = user.get('email')
    subject = 'User credentials!'
    message = 'Login: {login}, password: {password}'.format(login=email_to, password=user.get('password'))
    from_user = 'big.boss@bestSystem.chubaka'
    print(message)
    try:
        send_mail(subject, message, from_user, (email_to,))
    except Exception as e:
        print(e)
