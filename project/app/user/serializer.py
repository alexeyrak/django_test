import csv
import io

from rest_framework import serializers
from rest_framework.settings import api_settings

from .models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'email',)
        readonly = ('created_at', 'change_count', 'is_staff',)


class UserCreationSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        password = validated_data.pop('password', None)
        instance = self.Meta.model(**validated_data)
        if password is not None:
            instance.set_password(password)
        instance.save()
        return instance
    
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'password')


class UserImportSerializer(serializers.Serializer):
    file = serializers.FileField(allow_empty_file=False, required=True)

    def validate_file(self, file):
        return file

    def to_internal_value(self, data):
        print(data)
        file = data.get('file')
        try:
            parse = self.parse_file(file)
        except Exception:
            raise serializers.ValidationError({
                api_settings.NON_FIELD_ERRORS_KEY: ['Invalid csv file structure']
            })
        return {
            'file': parse,
            'base_file': file
        }

    def parse_file(self, csv_file):
        lines = csv.reader(io.StringIO(csv_file.read().decode('utf-8')))
        return [self.to_user_dict(fields) for fields in lines]

    def to_user_dict(self, fields):
        user_dict = {}
        user_dict['first_name'] = fields[0]
        user_dict['last_name'] = fields[1]
        user_dict['email'] = fields[2]
        return user_dict
