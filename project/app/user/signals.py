from django.db.models.signals import post_save
from django.dispatch import receiver
from app.user.models import User


@receiver(post_save, sender=User)
def change_user_handler(sender, instance, created, **kwargs):
    if not created:
        User.objects.filter(id=instance.id).update(change_count=instance.change_count + 1)
