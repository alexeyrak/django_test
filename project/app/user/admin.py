from django.contrib import admin
from .models import User
from django.contrib.auth.admin import UserAdmin as AuthUserAdmin


@admin.register(User)
class UserAdmin(AuthUserAdmin):
    change_list_template = 'admin/user/change_list.html'

    class Media:
        js = (
            'users.admin.js',
        )
        css = {
            'all': ('admin.css',)
        }

    ordering = ('email',)
    search_fields = ('first_name', 'last_name', 'email',)
    list_display = ('email', 'first_name', 'last_name', 'change_count', 'is_active', 'is_superuser', 'is_staff',
                    'created_at', 'id')

    fieldsets = (
        (None, {'fields': ('password',)}),
        ('Персональные данные', {'fields': ('first_name', 'last_name', 'email',)}),
        ('Права', {'fields': ('is_active', 'is_superuser', 'is_staff', 'groups')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )