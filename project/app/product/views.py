from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView

from .models import Product


class ProductsView(LoginRequiredMixin, ListView):
    model = Product
    template_name = 'app/products/list.html'
    paginate_by = 10
    ordering = ['-id']
    queryset = Product.objects.all()
