from django.db import models


class Product(models.Model):
    name = models.CharField(verbose_name='Product', max_length=50, null=False)
    price = models.DecimalField(max_digits=6, decimal_places=2)

    def __str__(self):
        return self.name
