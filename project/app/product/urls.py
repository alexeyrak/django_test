from django.conf.urls import url
from django.urls import path

from app.product.views import ProductsView

urlpatterns = [
    path('', ProductsView.as_view(), name='product_list')
]
