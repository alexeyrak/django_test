from __future__ import absolute_import
import os
from celery import Celery

from app import settings

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'app.settings')
os.environ.setdefault('DJANGO_CONFIGURATION', 'app.settings')

# import configurations
# configurations.setup()


class CeleryConf:
    BROKER_URL = 'amqp://{user}:{password}@rabbitmq:{port}{vhost}' \
        .format(user=os.environ['RABBITMQ_DEFAULT_USER'],
                password=os.environ['RABBITMQ_DEFAULT_PASS'],
                port=os.environ['RABBITMQ_DEFAULT_PORT'],
                vhost=os.environ['RABBITMQ_DEFAULT_VHOST'])

    CELERY_RESULT_BACKEND = 'amqp'
    CELERY_ACCEPT_CONTENT = ['json']
    CELERY_TASK_SERIALIZER = 'json'
    CELERY_RESULT_SERIALIZER = 'json'
    CELERY_IMPORTS = ('app.user.tasks',)


app = Celery('tasks')

# Using a string here means the worker will not have to
# pickle the object when using Windows.

app.config_from_object(CeleryConf)
# app.autodiscover_tasks(['app.user'])

