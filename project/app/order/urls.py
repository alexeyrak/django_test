from django.urls import path
from app.order.views import order_product_change, order_product_increase, TodayOrderView

urlpatterns = [
    path('change', order_product_change),
    path('increase', order_product_increase),
    path('list', TodayOrderView.as_view(), name='order'),
]
