from datetime import date
from rest_framework import serializers

from app.order.models import Order, OrderProduct
from app.product.models import Product
from app.user.models import User


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = '__all__'


class OrderProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderProduct
        fields = '__all__'


class OrderProductIncreaseSerializer(serializers.Serializer):
    product = serializers.PrimaryKeyRelatedField(queryset=Product.objects.all(), many=False, required=True)
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    # queryset=User.objects.all(),

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        order, order_created = Order.objects.get_or_create(user=validated_data.get('user'), date=date.today())
        order_product, created = OrderProduct.objects.get_or_create(product=validated_data.get('product'),
                                                                    order=order, defaults={'amount': 1})
        if not created:
            order_product.amount += 1
            order_product.save()
        return OrderProductSerializer(order_product).data


class OrderProductChangeSerializer(OrderProductIncreaseSerializer):
    amount = serializers.DecimalField(min_value=0, decimal_places=0, max_digits=3)

    def create(self, validated_data):
        self.amount = validated_data.get('amount')
        self.user = validated_data.get('user')
        self.product = validated_data.get('product')
        if not self.amount:
            return self.order_product_remove()

        else:
            return self.order_product_change()

    def order_product_change(self):
        order, order_created = Order.objects.get_or_create(user=self.user, date=date.today())
        order_product, created = OrderProduct.objects.get_or_create(product=self.product, order=order,
                                                                    defaults={'amount': self.amount})
        if created:
            order.orderproduct_set.add(order_product)
        else:
            order_product.amount = self.amount
            order_product.save()
        return OrderProductSerializer(order_product).data

    def order_product_remove(self):
        order_product = OrderProduct.objects.filter(product=self.product, order__date=date.today())
        if order_product:
            order_product.delete()
        return {}
