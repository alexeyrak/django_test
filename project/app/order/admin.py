from django.contrib import admin
from django.contrib.admin import AllValuesFieldListFilter

from app.order.models import Order, OrderProduct


class OrderProductInline(admin.TabularInline):
    model = OrderProduct


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    inlines = [
        OrderProductInline
    ]
    list_display = ('user_first_name', 'date',)
    list_filter = ('user__email', ('date', AllValuesFieldListFilter, ), )


    def user_first_name(self, obj):
        if obj.user.first_name or obj.user.last_name:
            return '{} {}'.format(obj.user.first_name, obj.user.last_name)
        else:
            return obj.user.email

    user_first_name.admin_order_field = 'user__first_name'

