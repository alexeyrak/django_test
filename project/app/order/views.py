from datetime import date

from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView
from rest_framework.decorators import api_view
from rest_framework.response import Response

from app.order.models import Order, OrderProduct
from app.order.serializer import OrderProductSerializer, OrderProductChangeSerializer, OrderProductIncreaseSerializer


class TodayOrderView(LoginRequiredMixin, ListView):
    login_url = '/auth/login/'
    model = OrderProduct
    template_name = 'app/order/list.html'
    ordering = ['-id']

    def get_queryset(self):
        return OrderProduct.objects.select_related('product')\
            .filter(order__user=self.request.user, order__date=date.today()).all()


@api_view(['post'])
def order_product_change(request):
    serializer = OrderProductChangeSerializer(data={
        'product': request.data.get('product'),
        'amount': request.data.get('amount'),
    }, context={
        "request": request,
    })
    serializer.is_valid(raise_exception=True)
    data = serializer.save()

    return Response(data)


@api_view(['post'])
def order_product_increase(request):
    serializer = OrderProductIncreaseSerializer(data={
        'product': request.data.get('product'),
    }, context={
        "request": request,
    })
    serializer.is_valid(raise_exception=True)
    data = serializer.save()
    print(data)
    return Response({})
