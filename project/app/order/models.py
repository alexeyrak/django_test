from django.db import models
from rest_framework.compat import MinValueValidator

from app.product.models import Product
from app.user.models import User


class Order(models.Model):
    user = models.ForeignKey(User, verbose_name='User', on_delete=models.CASCADE)
    date = models.DateField(verbose_name='Date', auto_now_add=True)
    class Meta:
        unique_together = ('user', 'date',)


class OrderProduct(models.Model):
    order = models.ForeignKey(Order, verbose_name='Order', on_delete=models.CASCADE)
    product = models.ForeignKey(Product, verbose_name='Product', on_delete=models.CASCADE)
    amount = models.PositiveIntegerField(verbose_name='Amount', validators=[MinValueValidator(1)])

    class Meta:
        unique_together = ('order', 'product',)
