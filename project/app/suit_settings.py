SUIT_CONFIG = {
    # header
    'ADMIN_NAME': 'Admin',
    # 'HEADER_DATE_FORMAT': 'l, j. F Y',
    # 'HEADER_TIME_FORMAT': 'H:i',

    # forms
    # 'SHOW_REQUIRED_ASTERISK': True,  # Default True
    # 'CONFIRM_UNSAVED_CHANGES': True, # Default True

    # menu
    'SEARCH_URL': '/admin/user/user/',
    # 'MENU_ICONS': {
    #    'sites': 'icon-leaf',
    #    'auth': 'icon-lock',
    # },
    # 'MENU_OPEN_FIRST_CHILD': True, # Default True
    # 'MENU_EXCLUDE': ('auth.group',),
    'MENU': (
        # 'sites',
        # {'app': 'auth', 'icon':'icon-lock', 'models': ('user', 'group')},
        {'label': 'Users', 'icon': 'icon-user', 'url': 'user.user'},
        {'label': 'Orders', 'icon':'icon-shopping-cart', 'url': 'order.order'},
        {'label': 'Products', 'icon':'icon-tags', 'url': 'product.product'},
        {'label': 'Site', 'icon':'icon-globe', 'url': '/'},
    ),

    # misc
    # 'LIST_PER_PAGE': 15
}